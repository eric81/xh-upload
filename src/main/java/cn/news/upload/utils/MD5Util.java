package cn.news.upload.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Util {

	/***
	 * MD5 加密
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 */
	public static String string2MD5(String inStr) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		return string2MD5(inStr, null);

	}
	/**
	 * MD5加密
	 * @param inStr
	 * @param charsetName
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 */
	public static String string2MD5(String inStr, String charsetName) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		
		MessageDigest md5 =  MessageDigest.getInstance("MD5");
		byte[] byteArray = null;
		if(charsetName != null){
			byteArray = inStr.getBytes(charsetName);
		}else{
			byteArray = inStr.getBytes();
		}
		byte[] md5Bytes = md5.digest(byteArray);
		StringBuffer hexValue = new StringBuffer();
		for (int i = 0; i < md5Bytes.length; i++) {
			int val = ((int) md5Bytes[i]) & 0xff;
			if (val < 16)
				hexValue.append("0");
			hexValue.append(Integer.toHexString(val));
		}
		return hexValue.toString();
	}

	/**
	 * 加密解密算法 执行一次加密，两次解密
	 */
	public static String convertMD5(String inStr) {

		char[] a = inStr.toCharArray();
		for (int i = 0; i < a.length; i++) {
			a[i] = (char) (a[i] ^ 't');
		}
		String s = new String(a);
		return s;

	}

}