package cn.news.upload.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import cn.news.upload.entities.XhUploadInfo;

public interface XhUploadInfoDao extends JpaRepository<XhUploadInfo, Long>, JpaSpecificationExecutor<XhUploadInfo>{
	Page<XhUploadInfo> findByTypeAndIsPass(String type, Integer isPass, Pageable pageable);
}
