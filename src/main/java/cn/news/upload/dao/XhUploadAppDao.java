package cn.news.upload.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.news.upload.entities.XhUploadApp;

public interface XhUploadAppDao extends JpaRepository<XhUploadApp, Long> {

}
