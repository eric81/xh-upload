package cn.news.upload.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.news.upload.entities.XhUploadInfo;
import cn.news.upload.service.InfoService;
import cn.news.upload.utils.JasonUtils;

@Controller
@RequestMapping("info")
public class InfoAction {
	@Autowired
	private InfoService infosv;

	/**
	 * 分页查询 <br/>
	 * 请求样例：/info/list?page=0&size=10&sort=id,desc&sort=name,asc
	 * 
	 * @param pageable
	 *            page: 第几页，从0开始，默认为第0页 <br/>
	 *            size: 每一页的大小，默认为10 <br/>
	 *            sort: 例如sort=id,desc&sort=lastname,asc表示在按id倒序排列基础上按name正序排列
	 * @return
	 */
	@GetMapping(value = "/list")
	public void list(XhUploadInfo info, HttpServletRequest request, HttpServletResponse response,
			@PageableDefault(value = 10, sort = { "id" }, direction = Sort.Direction.DESC) Pageable pageable) {

		Page<XhUploadInfo> rs = infosv.getInfoList(info, pageable);

		JasonUtils.writeJasonP(request, response, rs);
	}
}