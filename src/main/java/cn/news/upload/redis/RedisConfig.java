package cn.news.upload.redis;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import cn.news.upload.entities.XhUploadInfo;

@Configuration
public class RedisConfig {
	@Bean
	public RedisTemplate<String, XhUploadInfo> redisTemplate(RedisConnectionFactory factory) {
		RedisTemplate<String, XhUploadInfo> template = new RedisTemplate<String, XhUploadInfo>();
		template.setConnectionFactory(factory);
		template.setKeySerializer(new StringRedisSerializer());
		template.setValueSerializer(new RedisObjectSerializer());
		return template;
	}
}