package cn.news.upload.entities;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class XhUploadInfo extends BaseEntity {
	private static final long serialVersionUID = -3163607464644386905L;

	@Id
	@GeneratedValue
	private long id;

	@Column
	private int appId;

	@Column
	private String title;
	
	@Column
	private String descp;

	@Column
	private String type;

	@Column
	private Timestamp createTime;
	
	@Column
	private int isPass;
	
	@Column
	private String tag;

	@Column
	private String org;

	@Column
	private String district;

	@Column
	private String userName;

	@Column
	private String userRealname;

	@Column
	private String userNumber;

	@Column
	private String userTel;

	@Column
	private String userAddress;
	
	@Column
	private String userEmail;

	@Column
	private String userSex;

	@Column
	private String userAge;

	@Column
	private String userJob;

	@Column
	private String pic_url1;

	@Column
	private String pic_url2;

	@Column
	private String pic_url3;

	@Column
	private String pic_url4;

	@Column
	private String pic_url5;
	
	@Column
	private String pic_url6;
	
	@Column
	private String pic_url7;
	
	@Column
	private String pic_url8;
	
	@Column
	private String pic_url9;
	
	@Column
	private String pic_url10;

	@Column
	private String attach_url1;

	@Column
	private String attach_url2;

	@Column
	private String attach_url3;
	
	@Column
	private String attach_url4;
	
	@Column
	private String attach_url5;
	
	@Column
	private String video_fileName;
	
	@Column
	private String video_mp4Url;
	
	@Column
	private String video_flvUrl;
	
	@Column
	private String video_surfacePicUrl;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getAppId() {
		return appId;
	}

	public void setAppId(int appId) {
		this.appId = appId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescp() {
		return descp;
	}

	public void setDescp(String descp) {
		this.descp = descp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public int getIsPass() {
		return isPass;
	}

	public void setIsPass(int isPass) {
		this.isPass = isPass;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getOrg() {
		return org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserRealname() {
		return userRealname;
	}

	public void setUserRealname(String userRealname) {
		this.userRealname = userRealname;
	}

	public String getUserNumber() {
		return userNumber;
	}

	public void setUserNumber(String userNumber) {
		this.userNumber = userNumber;
	}

	public String getUserTel() {
		return userTel;
	}

	public void setUserTel(String userTel) {
		this.userTel = userTel;
	}

	public String getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserSex() {
		return userSex;
	}

	public void setUserSex(String userSex) {
		this.userSex = userSex;
	}

	public String getUserAge() {
		return userAge;
	}

	public void setUserAge(String userAge) {
		this.userAge = userAge;
	}

	public String getUserJob() {
		return userJob;
	}

	public void setUserJob(String userJob) {
		this.userJob = userJob;
	}

	public String getPic_url1() {
		return pic_url1;
	}

	public void setPic_url1(String pic_url1) {
		this.pic_url1 = pic_url1;
	}

	public String getPic_url2() {
		return pic_url2;
	}

	public void setPic_url2(String pic_url2) {
		this.pic_url2 = pic_url2;
	}

	public String getPic_url3() {
		return pic_url3;
	}

	public void setPic_url3(String pic_url3) {
		this.pic_url3 = pic_url3;
	}

	public String getPic_url4() {
		return pic_url4;
	}

	public void setPic_url4(String pic_url4) {
		this.pic_url4 = pic_url4;
	}

	public String getPic_url5() {
		return pic_url5;
	}

	public void setPic_url5(String pic_url5) {
		this.pic_url5 = pic_url5;
	}

	public String getPic_url6() {
		return pic_url6;
	}

	public void setPic_url6(String pic_url6) {
		this.pic_url6 = pic_url6;
	}

	public String getPic_url7() {
		return pic_url7;
	}

	public void setPic_url7(String pic_url7) {
		this.pic_url7 = pic_url7;
	}

	public String getPic_url8() {
		return pic_url8;
	}

	public void setPic_url8(String pic_url8) {
		this.pic_url8 = pic_url8;
	}

	public String getPic_url9() {
		return pic_url9;
	}

	public void setPic_url9(String pic_url9) {
		this.pic_url9 = pic_url9;
	}

	public String getPic_url10() {
		return pic_url10;
	}

	public void setPic_url10(String pic_url10) {
		this.pic_url10 = pic_url10;
	}

	public String getAttach_url1() {
		return attach_url1;
	}

	public void setAttach_url1(String attach_url1) {
		this.attach_url1 = attach_url1;
	}

	public String getAttach_url2() {
		return attach_url2;
	}

	public void setAttach_url2(String attach_url2) {
		this.attach_url2 = attach_url2;
	}

	public String getAttach_url3() {
		return attach_url3;
	}

	public void setAttach_url3(String attach_url3) {
		this.attach_url3 = attach_url3;
	}

	public String getAttach_url4() {
		return attach_url4;
	}

	public void setAttach_url4(String attach_url4) {
		this.attach_url4 = attach_url4;
	}

	public String getAttach_url5() {
		return attach_url5;
	}

	public void setAttach_url5(String attach_url5) {
		this.attach_url5 = attach_url5;
	}

	public String getVideo_fileName() {
		return video_fileName;
	}

	public void setVideo_fileName(String video_fileName) {
		this.video_fileName = video_fileName;
	}

	public String getVideo_mp4Url() {
		return video_mp4Url;
	}

	public void setVideo_mp4Url(String video_mp4Url) {
		this.video_mp4Url = video_mp4Url;
	}

	public String getVideo_flvUrl() {
		return video_flvUrl;
	}

	public void setVideo_flvUrl(String video_flvUrl) {
		this.video_flvUrl = video_flvUrl;
	}

	public String getVideo_surfacePicUrl() {
		return video_surfacePicUrl;
	}

	public void setVideo_surfacePicUrl(String video_surfacePicUrl) {
		this.video_surfacePicUrl = video_surfacePicUrl;
	}
}