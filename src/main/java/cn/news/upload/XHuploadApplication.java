package cn.news.upload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XHuploadApplication {

	public static void main(String[] args) {
		SpringApplication.run(XHuploadApplication.class, args);
	}
}
